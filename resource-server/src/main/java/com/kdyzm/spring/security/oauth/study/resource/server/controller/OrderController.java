package com.kdyzm.spring.security.oauth.study.resource.server.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kdyzm.spring.security.oauth.study.resource.server.dto.UserDetailsExpand;
import com.kdyzm.spring.security.oauth.study.resource.server.intercepter.AuthContextHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

/**
 * @author kdyzm
 */
@RestController
@Slf4j
public class OrderController {

    @Autowired
    private ObjectMapper objectMapper;

    @GetMapping("/r1")
    @PreAuthorize("hasAnyAuthority('p1')")
    public UserDetailsExpand r1(HttpServletRequest httpServletRequest) throws JsonProcessingException {
//        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//        Object principal = authentication.getPrincipal();
//        return objectMapper.writeValueAsString(principal);

        UserDetailsExpand context = AuthContextHolder.getInstance().getContext();
        return context;
    }
}
